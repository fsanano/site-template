'use strict';

// gulp plugins
var gulp		= require('gulp'),
stylus			= require('gulp-stylus'),
autoprefixer	= require('gulp-autoprefixer'),
sourcemaps 		= require('gulp-sourcemaps'),


jshint 			= require('gulp-jshint'),
connect 		= require('gulp-connect'),

usemin 			= require('gulp-usemin'),
gutil 			= require('gulp-util'),
rename 			= require('gulp-rename'),

uglify 			= require('gulp-uglify'),
minifyHtml 		= require('gulp-minify-html'),
minifyCss 		= require('gulp-minify-css'),
rev 			= require('gulp-rev'),

imagemin 		= require('gulp-imagemin'),
tinypng 		= require('gulp-tinypng'),

git 			= require('git-rev'),
zip 			= require('gulp-zip'),

plumber 		= require('gulp-plumber'),
bs 				= require('browser-sync').create(),
del 			= require('del'),
newer			= require('gulp-newer'),
promise 		= require('es6-promise').polyfill();

var branch = 'branch';



gulp.task('zip', function(){
	setTimeout(function(){
		return gulp.src( 'public/' + branch + '**/*', {read: false})
			.pipe( zip( 'public/' + branch ) )
			.pipe(gulp.dest('./'));
	}, 200);
});


gulp.task('connect', function(){
	bs.init({
		server: "./app",
		livereload: true
	});
});


gulp.task('html', function () {
	return gulp.src('./app/*.html')
		.pipe(connect.reload());
});


gulp.task('stylus', function () {
	return gulp.src(['app/styl/main.styl', 'app/styl/first.styl'])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(stylus({
			compress: true,
			onError: function (error) {
				gutil.log(gutil.colors.red(error));
				gutil.beep();
			},
			onSuccess: function () {
				gutil.log(gutil.colors.green('Stylus compiled successfully.'));
			}
		}))
		
		.pipe(sourcemaps.write())
		.pipe(autoprefixer({
			browsers: ['last 3 versions']
		}))
		.pipe(gulp.dest('./app/'))
});


gulp.task('js',function () {
	return gulp.src('app/js/main.js')
		.pipe(plumber())
		// .pipe(jshint())
		.pipe(gulp.dest('app/js'))

});



gulp.task('watch', function () {
	gulp.watch([ './app/styl/*.styl'], gulp.series('stylus', bs.reload));
	gulp.watch([ './app/js/*.js'], gulp.series(bs.reload));
	gulp.watch([ './app/*.html'], bs.reload);
});

gulp.task('default', gulp.series('stylus', 'js', gulp.parallel('watch', 'connect')));


gulp.task('clean', function(cb){
	git.branch(function (str) {
		branch = str;
		del('public/' + branch +'/*.*')
		cb();
	});
})


gulp.task('usemin', function(cb) {
	git.branch(function (str) {
		branch = str;
		return gulp.src('./app/*.html')
			.pipe(usemin({
				css: [ rev() ],
				html: [ function () {return minifyHtml({ empty: true });} ],
				js: [ uglify, rev() ],
				inlinejs: [ uglify ],
				inlinecss: [ minifyCss, 'concat' ]
			}))
			.pipe(gulp.dest( 'public/' + branch ));
	});
	cb();
});



gulp.task('fonts', function(){
	return gulp.src(['app/fonts/*'] ,{read: false}).pipe(gulp.dest( 'public/' + branch + '/fonts'));
});








